# LDAS Tools Diskcache Service image

This image provides a running containerised version of the
LDAS Tools Diskcache service.

To run it, publish the relevant port to the host, and bind mount your
`diskcache.rsc` configuration file and any volumes that are configured to
be scanned by the diskcache daemon:

```shell
docker run \
  -p 20222:20222 \
  -v $(pwd)/diskcache.rsc:/etc/diskcache/diskcache.rsc \
  -v /cvmfs:/cvmfs \
  -v /data:/data \
  igwn/diskcache:2.7.7
```

The diskcache service may take a couple of minutes to perform an initial scan,
but then should be queriable using the `diskcache dump` command-line interface:

```shell
diskcache --host 127.0.0.1 --port 20222 dump --output-ascii -
```
